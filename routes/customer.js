var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var customer = require('../models/customer.js');

// GET all customers
router.get('/', function(req, res, next) {
  customer.find(function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

// GET SINGLE customer BY ID
router.get('/:id', function(req, res, next) {
  customer.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

// SAVE customer
router.post('/', function(req, res, next) {
  customer.create(req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

// UPDATE customer
router.put('/:id', function(req, res, next) {
  customer.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

// DELETE customer
router.delete('/:id', function(req, res, next) {
  customer.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;
