var mongoose = require('mongoose');
var schema = mongoose.Schema;

var customerSchema = schema({
  id_number: Number,
  first_name: String,
  surname: String,
  contact_information: {email: String, phone: Number, address: String},
  updated_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model('customer', customerSchema);
