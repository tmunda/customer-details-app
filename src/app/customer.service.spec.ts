/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { customerService } from './customer.service';

describe('customerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [customerService]
    });
  });

  it('should ...', inject([customerService], (service: customerService) => {
    expect(service).toBeTruthy();
  }));
});
