import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { customerService } from '../customer.service';

@Component({
  selector: 'app-customer-create',
  templateUrl: './customer-create.component.html',
  styleUrls: ['./customer-create.component.css']
})
export class customerCreateComponent implements OnInit {

  customer = {};

  constructor(private customerService: customerService, private router: Router) { }

  ngOnInit() {
  }

  savecustomer() {
    this.customerService.saveCustomer(this.customer).then((result) => {
      let id = result['_id'];
      this.router.navigate(['/customer-details', id]);
    }, (err) => {
      console.log(err);
    });
  }

}
