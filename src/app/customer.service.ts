import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class customerService {

  constructor(private http: Http) { }

  getAllCustomers() {
    return new Promise((resolve, reject) => {
      this.http.get('/customer')
        .map(res => res.json())
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  showCustomer(id) {
    console.log('id::', id);
    return new Promise((resolve, reject) => {
        this.http.get('/customer/' + id)
          .map(res => res.json())
          .subscribe(res => {
            resolve(res)
        }, (err) => {
          reject(err);
        });
    });
  }

  saveCustomer(data) {
    return new Promise((resolve, reject) => {
        this.http.post('/customer', data)
          .map(res => res.json())
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
    });
  }

  updateCustomer(id, data) {
    return new Promise((resolve, reject) => {
        this.http.put('/customer/'+id, data)
          .map(res => res.json())
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
    });
  }

  deleteCustomer(id) {
    return new Promise((resolve, reject) => {
        this.http.delete('/customer/'+id)
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });
    });
  }

}
