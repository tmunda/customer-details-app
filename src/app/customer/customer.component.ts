import { Component, OnInit } from '@angular/core';
import { customerService } from '../customer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class customerComponent implements OnInit {

  customers: any;

  constructor(private customerService: customerService) { }

  ngOnInit() {
    this.getcustomerList();
  }

  getcustomerList() {
    this.customerService.getAllCustomers().then((res) => {
      this.customers = res;
      console.log(this.customers);
    }, (err) => {
      console.log(err);
    });
  }

}
