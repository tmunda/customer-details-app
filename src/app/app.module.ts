import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { AppComponent } from './app.component';
import { customerComponent } from './customer/customer.component';
import { customerService } from './customer.service';
import { customerDetailComponent } from './customer-detail/customer-detail.component';
import { customerCreateComponent } from './customer-create/customer-create.component';
import { customerEditComponent } from './customer-edit/customer-edit.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'customers', pathMatch: 'full' },
  { path: 'customers', component: customerComponent },
  { path: 'customer-details/:id', component: customerDetailComponent },
  { path: 'customer-create', component: customerCreateComponent },
  { path: 'customer-edit/:id', component: customerEditComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    customerComponent,
    customerDetailComponent,
    customerCreateComponent,
    customerEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    customerService,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
