import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { customerService } from '../customer.service';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
})
export class customerDetailComponent implements OnInit {

  customer = {};

  constructor(private route: ActivatedRoute, private router: Router, private customerService: customerService) { }

  ngOnInit() {
    this.getCustomerDetail(this.route.snapshot.params['id']);
  }

  getCustomerDetail(id) {
    this.customerService.showCustomer(id).then((res) => {
      console.log('res::', res);
      this.customer = res;
      console.log(this.customer);
    }, (err) => {
      console.log(err);
    });
  }

  deleteCustomer(id) {
    this.customerService.deleteCustomer(id).then((result) => {
      this.router.navigate(['/customers']);
    }, (err) => {
      console.log(err);
    });
  }

}
