import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { customerService } from '../customer.service';

@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.css']
})
export class customerEditComponent implements OnInit {

  customer = {};

  constructor(private customerService: customerService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getcustomer(this.route.snapshot.params['id']);
  }

  getcustomer(id) {
    this.customerService.showCustomer(id).then((res) => {
      this.customer = res;
      console.log(this.customer);
    }, (err) => {
      console.log(err);
    });
  }

  updatecustomer(id) {
    this.customerService.updateCustomer(id, this.customer).then((result) => {
      let id = result['_id'];
      this.router.navigate(['/customer-details', id]);
    }, (err) => {
      console.log(err);
    });
  }

}
